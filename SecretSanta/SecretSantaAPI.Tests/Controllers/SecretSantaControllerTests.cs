﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using SecretSanta.Controllers;
using SecretSantaAPI.Models;
using SecretSantaContracts;
using SecretSantaCore;
using TestsData;

namespace SecretSantaAPI.Tests.Controllers
{
    [TestFixture]
    public class SecretSantaControllerTests
    {
        private SecretSantaLogic logic;
        private SecretSantaController controller;
        private ILogger<SecretSantaController> logger;

        [SetUp]
        public void SetUp()
        {
            this.logger = Mock.Of<ILogger<SecretSantaController>>();
            this.logic = new SecretSantaLogic();
            this.controller = new SecretSantaController(logger, this.logic);
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void PostCalculateSecretSantaPairs_CompareSendersListWithNamesList_ListsAreEqual(string[] names)
        {
            //Arrange
            SecretSantaNamesModel model = new SecretSantaNamesModel()
            {
                Names = names
            };

            //Act
            var result = this.controller.PostCalculateSecretSantaPairs(model);
            var okObjectResult = result as OkObjectResult;
            var sendersList = ((IList<ISecretSantaPair>)okObjectResult.Value)
                .Select(pair => pair.Sender).ToList();

            //Assert
            CollectionAssert.AreEqual(names, sendersList, "List of senders is not the same as list of original names.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void PostCalculateSecretSantaPairs_CompareSortedListOfReceiversWithSortedListOfNames_ListsAreEqual(string[] names)
        {
            //Arrange
            SecretSantaNamesModel model = new SecretSantaNamesModel()
            {
                Names = names
            };

            //Act
            var result = this.controller.PostCalculateSecretSantaPairs(model);
            var okObjectResult = result as OkObjectResult;
            var receiversList = ((IList<ISecretSantaPair>)okObjectResult.Value)
                .Select(pair => pair.Receiver).ToList();
            List<string> expectedListOfNames = new List<string>(names);
            expectedListOfNames.Sort();
            receiversList.Sort();

            //Assert
            CollectionAssert.AreEqual(
                expectedListOfNames, 
                receiversList, 
                "Sorted list of receivers is not the same as list of original names.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void PostCalculateSecretSantaPairs_CompareActualListOfReceiversWithListOfNames_ListOfReceiversIsNotEqualToNames(string[] names)
        {
            //Arrange
            SecretSantaNamesModel model = new SecretSantaNamesModel()
            {
                Names = names
            };

            //Act
            var result = this.controller.PostCalculateSecretSantaPairs(model);
            var okObjectResult = result as OkObjectResult;
            var receiversList = ((IList<ISecretSantaPair>)okObjectResult.Value)
                .Select(pair => pair.Receiver).ToList();

            //Assert
            CollectionAssert.AreNotEqual(names, receiversList, "List of receivers should not be the same as list of original names.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void PostCalculateSecretSantaPairs_CompareSenderAndReceiverInPairs_SenderAndReceiverShouldNotBeTheSameName(string[] names)
        {
            //Arrange
            SecretSantaNamesModel model = new SecretSantaNamesModel()
            {
                Names = names
            };

            //Act
            var result = this.controller.PostCalculateSecretSantaPairs(model);
            var okObjectResult = result as OkObjectResult;
            var pairs = ((IList<ISecretSantaPair>)okObjectResult.Value);

            //Assert
            foreach (var pair in pairs)
            {
                if (pair.Sender.Equals(pair.Receiver))
                {
                    Assert.Fail($"{pair.Sender} is both sender and receiver in pair.");
                }
            }
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfNamesWithNull))]
        public void PostCalculateSecretSantaPairs_NamesListContainsNull_ThrowArgumentNullException(string[] names)
        {
            //Arrange
            SecretSantaNamesModel model = new SecretSantaNamesModel()
            {
                Names = names
            };

            //Act
            var result = this.controller.PostCalculateSecretSantaPairs(model);
            var badRequestObjectResult = result as BadRequestObjectResult;

            //Assert
            Assert.AreEqual(
                StatusCodes.Status400BadRequest, 
                badRequestObjectResult.StatusCode.Value, 
                "Null instead of a name should lead to status code 400.");
        }

        [Test]
        public void PostCalculateSecretSantaPairs_NamesListIsNull_ThrowArgumentNullException()
        {
            //Arrange
            SecretSantaNamesModel model = new SecretSantaNamesModel()
            {
                Names = null
            };

            //Act
            var result = this.controller.PostCalculateSecretSantaPairs(model);
            var badRequestObjectResult = result as BadRequestObjectResult;

            //Assert
            Assert.AreEqual(
                StatusCodes.Status400BadRequest,
                badRequestObjectResult.StatusCode.Value,
                "Null instead of a names list should lead to status code 400.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfNamesWithLessThenTwoNames))]
        public void PostCalculateSecretSantaPairs_SendLessThenTwoNames_ThrowArgumentException(string[] names)
        {
            //Arrange
            SecretSantaNamesModel model = new SecretSantaNamesModel()
            {
                Names = names
            };

            //Act
            var result = this.controller.PostCalculateSecretSantaPairs(model);
            var badRequestObjectResult = result as BadRequestObjectResult;

            //Assert
            Assert.AreEqual(
                StatusCodes.Status400BadRequest,
                badRequestObjectResult.StatusCode.Value,
                "Getting less then two names should lead to status code 400.");
        }
    }
}
