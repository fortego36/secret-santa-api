﻿using NUnit.Framework;
using SecretSantaAPI.Attributes;
using TestsData;

namespace SecretSantaAPI.Tests
{
    [TestFixture]
    public class PreventNamesDuplicatesAttributeTests
    {
        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfNamesWithDuplicates))]
        public void IsValid_NamesListContainsDuplicates_ReturnFalse(string[] names)
        {
            //Arrange
            var attribute = new PreventNamesDuplicatesAttribute();

            //Act
            var result = attribute.IsValid(names);

            //Assert
            Assert.That(result, Is.False, "List of names with duplicates should not be valid.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void IsValid_SendCorrectListOfNames_ReturnTrue(string[] names)
        {
            //Arrange
            var attribute = new PreventNamesDuplicatesAttribute();

            //Act
            var result = attribute.IsValid(names);

            //Assert
            Assert.That(result, Is.True, "Correct list of names without duplicates should be valid.");
        }
    }
}
