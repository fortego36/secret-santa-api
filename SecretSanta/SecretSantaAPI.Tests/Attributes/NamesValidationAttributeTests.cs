﻿using NUnit.Framework;
using SecretSantaAPI.Attributes;
using TestsData;

namespace SecretSantaAPI.Tests.Attributes
{
    [TestFixture]
    public class NamesValidationAttributeTests
    {
        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfNamesWithEmptyStringAndWhiteSpaces))]
        public void IsValid_NamesListContainsEmptyStringOrWhiteSpaces_ReturnFalse(string[] names)
        {
            //Arrange
            var attribute = new NamesValidationAttribute();

            //Act
            var result = attribute.IsValid(names);

            //Assert
            Assert.That(result, Is.False, "List of names with empty string or null should not be valid.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void IsValid_SendCorrectListOfNames_ReturnTrue(string[] names)
        {
            //Arrange
            var attribute = new NamesValidationAttribute();

            //Act
            var result = attribute.IsValid(names);

            //Assert
            Assert.That(result, Is.True, "Correct list of names should be valid.");
        }
    }
}
