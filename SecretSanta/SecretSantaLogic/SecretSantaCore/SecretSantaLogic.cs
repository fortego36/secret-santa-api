﻿using System;
using System.Collections.Generic;
using SecretSantaContracts;

namespace SecretSantaCore
{
    /// <summary>
    /// Custom implementation of Secret Santa service logic.
    /// </summary>
    public class SecretSantaLogic : ISecretSantaLogic
    {
        /// <inheritdoc/>
        public IList<ISecretSantaPair> GetPairs(IList<string> names)
        {
            SecretSantaNamesValidator.Validate(names);
            (int, int)[] pairsIndexes = GetPairsIndexes(names.Count);
            List<ISecretSantaPair> list = new List<ISecretSantaPair>();
            foreach (var pair in pairsIndexes)
            {
                list.Add(new SecretSantaPair()
                {
                    Sender = names[pair.Item1],
                    Receiver = names[pair.Item2]
                });
            }
            return list;
        }

        private void SwapReceivers(int firstSenderIndex, int secondSenderIndex, (int,int)[] pairs)
        {
            int temp = pairs[firstSenderIndex].Item2;
            pairs[firstSenderIndex].Item2 = pairs[secondSenderIndex].Item2;
            pairs[secondSenderIndex].Item2 = temp;
        }

        private (int, int)[] GetPairsIndexes(int namesCount)
        {
            Random rand = new Random();
            (int, int)[] pairsIndexes = new (int, int)[namesCount];
            for (int index = 0; index < namesCount; ++index)
            {
                pairsIndexes[index].Item1 = index;
                pairsIndexes[index].Item2 = index;
            }

            for (int currentSenderIndex = 0; currentSenderIndex < namesCount - 1; ++currentSenderIndex)
            {
                int availableReceiverIndex = rand.Next(namesCount - 1 - currentSenderIndex) + 1 + currentSenderIndex;
                SwapReceivers(currentSenderIndex, availableReceiverIndex, pairsIndexes);
            }

            if (pairsIndexes[namesCount - 1].Item1 == pairsIndexes[namesCount - 1].Item2)
            {
                int randomSenderIndexExceptTheLastOne = rand.Next(namesCount - 1);
                SwapReceivers(randomSenderIndexExceptTheLastOne, namesCount - 1, pairsIndexes);
            }

            return pairsIndexes;
        }
    }
}
