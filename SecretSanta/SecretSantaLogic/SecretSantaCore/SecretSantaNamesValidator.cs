﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SecretSantaCore
{
    internal static class SecretSantaNamesValidator
    {
        /// <summary>
        /// Validation for a list of names for Secret Santa service.
        /// </summary>
        /// <param name="names">List of names</param>
        /// <exception cref="ArgumentNullException">Thrown when list of names is null or contains null.</exception>
        /// <exception cref="ArgumentException">Thrown when names count is less then two.</exception>
        internal static void Validate(IList<string> names)
        {
            if (names is null)
            {
                throw new ArgumentNullException(nameof(names));
            }

            if (names.Count < 2)
            {
                throw new ArgumentException("Secret Santa service require at least 2 items.");
            }

            if (names.Any(item => item is null))
            {
                throw new ArgumentNullException(nameof(names), "One of the items in list is null.");
            }
        }
    }
}
