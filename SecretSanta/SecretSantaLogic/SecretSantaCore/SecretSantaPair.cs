﻿using SecretSantaContracts;

namespace SecretSantaCore
{
    /// <summary>
    /// Class contains names of sender and receiver for Secret Santa service.
    /// </summary>
    public class SecretSantaPair : ISecretSantaPair
    {
        /// <inheritdoc/>
        public string Sender { get; set; }

        /// <inheritdoc/>
        public string Receiver { get; set; }
    }
}
