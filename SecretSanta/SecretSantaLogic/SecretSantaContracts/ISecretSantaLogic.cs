﻿using System.Collections.Generic;

namespace SecretSantaContracts
{
    /// <summary>
    /// Interface for Secret Santa service logic.
    /// </summary>
    public interface ISecretSantaLogic
    {
        /// <summary>
        /// Convert list of participants to list of groups with sender and receiver.
        /// </summary>
        /// <param name="names">List of partisipants.</param>
        /// <returns>List of groups, where every group has sender and receiver.</returns>
        public IList<ISecretSantaPair> GetPairs(IList<string> names);
    }
}
