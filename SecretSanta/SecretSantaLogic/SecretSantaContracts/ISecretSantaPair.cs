﻿namespace SecretSantaContracts
{
    /// <summary>
    /// Interface for class with information about pairs in Secret Santa service.
    /// </summary>
    public interface ISecretSantaPair
    {
        /// <summary>
        /// Gets or sets name of person, who will be a Secret Santa.
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// Gets or sets name of person, who will receive a present from sender.
        /// </summary>
        public string Receiver { get; set; }
    }
}
