﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TestsData;

namespace SecretSantaCore.Tests
{
    [TestFixture]
    public class SecretSantaLogicTests
    {
        private SecretSantaLogic logic;

        [SetUp]
        public void SetUp()
        {
            this.logic = new SecretSantaLogic();
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void GetPairs_CompareSendersListWithNamesList_ListsAreEqual(string[] names)
        {
            //Act
            var pairs = logic.GetPairs(names);
            var senders = pairs.Select(pair => pair.Sender).ToArray();

            //Assert
            CollectionAssert.AreEqual(names, senders, "List of senders is not the same as list of original names.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void GetPairs_CompareSortedListOfReceiversWithSortedListOfNames_ListsAreEqual(string[] names)
        {
            //Act
            var pairs = logic.GetPairs(names);
            var receivers = pairs.Select(pair => pair.Receiver).ToList();
            List<string> expectedListOfNames = new List<string>(names);
            expectedListOfNames.Sort();
            receivers.Sort();

            //Assert
            CollectionAssert.AreEqual(expectedListOfNames, receivers, "Sorted list of receivers is not the same as list of original names.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void GetPairs_CompareActualListOfReceiversWithListOfNames_ListOfReceiversIsNotEqualToNames(string[] names)
        {
            //Act
            var pairs = logic.GetPairs(names);
            var receivers = pairs.Select(pair => pair.Receiver).ToList();

            //Assert
            CollectionAssert.AreNotEqual(names, receivers, "List of receivers should not be the same as list of original names.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfDifferentNames))]
        public void GetPairs_CompareSenderAndReceiverInPairs_SenderAndReceiverShouldNotBeTheSameName(string[] names)
        {
            //Act
            var pairs = logic.GetPairs(names);

            //Assert
            foreach (var pair in pairs)
            {
                if (pair.Sender.Equals(pair.Receiver))
                {
                    Assert.Fail($"{pair.Sender} is both sender and receiver in pair.");
                }
            }
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfNamesWithNull))]
        public void GetPairs_NamesListContainsNull_ThrowArgumentNullException(string[] names)
        {
            //Assert
            Assert.Throws<ArgumentNullException>(() => logic.GetPairs(names), "Null instead of a name should lead to ArgumentNullException.");
        }

        [Test]
        public void GetPairs_NamesListIsNull_ThrowArgumentNullException()
        {
            //Assert
            Assert.Throws<ArgumentNullException>(() => logic.GetPairs(null), "Null instead of names list should lead to ArgumentNullException.");
        }

        [TestCaseSource(typeof(SecretSantaTestsData), nameof(SecretSantaTestsData.ListOfNamesWithLessThenTwoNames))]
        public void GetPairs_SendLessThenTwoNames_ThrowArgumentException(string[] names)
        {
            //Assert
            Assert.Throws<ArgumentException>(() => logic.GetPairs(names), "Getting less then two names should lead to ArgumentException.");
        }
    }
}
