﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using SecretSantaAPI.Attributes;

namespace SecretSantaAPI.Models
{
    public class SecretSantaNamesModel
    {
        [BindProperty]
        [Required]
        [NamesValidation]
        [PreventNamesDuplicates]
        public IList<string> Names { get; set; }
    }
}
