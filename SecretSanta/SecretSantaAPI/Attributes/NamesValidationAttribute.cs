﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SecretSantaAPI.Attributes
{
    public class NamesValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var names = (IList<string>)value;
            if (names.Any(name => string.IsNullOrWhiteSpace(name)))
            {
                return new ValidationResult($"Names should not be null, empty or whitespace");
            }

            return ValidationResult.Success;
        }
    }
}
