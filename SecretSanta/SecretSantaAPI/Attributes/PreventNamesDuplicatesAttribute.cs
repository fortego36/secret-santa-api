﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SecretSantaAPI.Attributes
{
    public class PreventNamesDuplicatesAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var names = (IList<string>)value;
            var duplicatedNames = GetDuplicates(names);
            if (duplicatedNames.Count > 0)
            {
                return new ValidationResult($"Names list contains duplicates: {string.Join(", ", duplicatedNames)}");
            }

            return ValidationResult.Success;
        }

        private IList<string> GetDuplicates(IList<string> names)
        {
            var duplicates = names.GroupBy(g => g)
                .Where(group => group.Count() > 1)
                .Select(group => group.Key).ToList();
            return duplicates;
        }
    }
}
