﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SecretSantaAPI.Models;
using SecretSantaContracts;

namespace SecretSanta.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SecretSantaController : ControllerBase
    {
        private readonly ISecretSantaLogic secretSantaImplementation;
        private readonly ILogger<SecretSantaController> logger;

        public SecretSantaController(ILogger<SecretSantaController> logger, ISecretSantaLogic secretSantaImplementation)
        {
            this.logger = logger;
            this.secretSantaImplementation = secretSantaImplementation;
        }

        [HttpPost]
        public ActionResult PostCalculateSecretSantaPairs([FromBody]SecretSantaNamesModel model)
        {
            try
            {
                logger.LogInformation($"New request. Names list: {string.Join(", ", model.Names)}");
                var pairs = secretSantaImplementation.GetPairs(model.Names);
                logger.LogInformation($"Calculated pairs list: {string.Join("; ", pairs.Select(pair => $"{Environment.NewLine}Sender: {pair.Sender}, Receiver: {pair.Receiver}"))}");
                return Ok(pairs);
            }
            catch (ArgumentException ex)
            {
                logger.LogWarning(ex.Message);
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
