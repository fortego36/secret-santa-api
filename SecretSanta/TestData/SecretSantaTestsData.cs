﻿using System.Collections.Generic;
using NUnit.Framework;

namespace TestsData
{
    public static class SecretSantaTestsData
    {
        public static IEnumerable<TestCaseData> ListOfDifferentNames
        {
            get
            {
                yield return new TestCaseData
                    (
                        arg: new[] { "Ivan", "Stew", "Mat", "Igor", "Denny" }
                    );
                yield return new TestCaseData
                    (
                        arg: new[] { "Ivan", "Stew" }
                    );
            }
        }

        public static IEnumerable<TestCaseData> ListOfNamesWithNull
        {
            get
            {
                yield return new TestCaseData
                    (
                        arg: new[] { "Ivan", "Stew", null, "Igor", "Denny" }
                    );
            }
        }

        public static IEnumerable<TestCaseData> ListOfNamesWithLessThenTwoNames
        {
            get
            {
                yield return new TestCaseData
                    (
                        arg: new[] { "Ivan" }
                    );
                yield return new TestCaseData
                    (
                        arg: new string[0]
                    );
            }
        }

        public static IEnumerable<TestCaseData> ListOfNamesWithEmptyStringAndWhiteSpaces
        {
            get
            {
                yield return new TestCaseData
                    (
                        arg: new[] { "Ivan", "Stew", "", "Igor", "Denny" }
                    );
                yield return new TestCaseData
                    (
                        arg: new[] { "Ivan", " ", "Mat", "     ", "Denny" }
                    );
            }
        }

        public static IEnumerable<TestCaseData> ListOfNamesWithDuplicates
        {
            get
            {
                yield return new TestCaseData
                    (
                        arg: new[] { "Ivan", "Stew", "Igor", "Igor", "Denny", "Ivan" }
                    );
            }
        }
    }
}
