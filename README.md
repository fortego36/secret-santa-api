# secret-santa-api

You can find an example of API request in ```SecretSanta.postman_collection.json``` for Postman in project folder.

Current API URL: https://localhost:44376/api/secretsanta

API require a POST request with "Names" argument in request body as the collection of strings (in JSON formatting).

Request body example:

```
{
    "Names": [
		"Igor",
		"Mat",
		"Viktor",
		"Andrew",
		"StackOverflow",
		"Google"
		]
}
```

API provides you a collection of pairs: gift senders and gift receivers.

Response example:

```
[
    {
        "sender": "Igor",
        "receiver": "Google"
    },
    {
        "sender": "Mat",
        "receiver": "Viktor"
    },
    {
        "sender": "Viktor",
        "receiver": "Igor"
    },
    {
        "sender": "Andrew",
        "receiver": "Mat"
    },
    {
        "sender": "StackOverflow",
        "receiver": "Andrew"
    },
    {
        "sender": "Google",
        "receiver": "StackOverflow"
    }
]
```

Possible ways to receive status code 400:
 - Sending empty string or whitespaces instead of names.
 - Sending a collection with less then two names.
 - Sending nothing (or something that would not be parsed as IList<string>).
 - Sending duplicated names.

API returns status code 500 if you find a way to crash it.